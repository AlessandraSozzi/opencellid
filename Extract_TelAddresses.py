import pymongo
import pandas as pd

db = pymongo.MongoClient("192.168.0.98:30001")["twitter"]["address"]

tel_addresses = db.find({"classification.full": "CU06"})

df = pd.DataFrame()

for doc in tel_addresses:
    frame = pd.DataFrame({  'Northing': [doc['coordinates'][0]],
                            'Easting': [doc['coordinates'][1]],
                            'oslaua': [doc['levels']['oslaua']],
                            'msoa11': [doc['levels']['msoa11']],
                            'postcode': [doc['postcode']] })
    df = df.append(frame)

df.to_csv('mongo_towers.csv', index = False)

cell_tw = pd.read_csv('/Users/Alessandra/GitHub/OpenCellID/MongoData/mongo_towers.csv', header = 0)

# Lat, long convertion
from pyproj import Proj, transform

bng = Proj(init = 'epsg:27700')
wgs84 = Proj(init = 'epsg:4326')

lon,lat = transform(bng, wgs84, cell_tw['Easting'].values, cell_tw['Northing'].values)
cell_tw['lon'] = lon
cell_tw['lat'] = lat

cell_tw.to_csv('mongo_towers2.csv', index = False)

cell_tw = pd.read_csv('/Users/Alessandra/GitHub/OpenCellID/MongoData/mongo_towers2.csv', header = 0)

cell_tw.plot(x = 'Easting', y = 'Northing', kind='scatter')

# Harvesine Formula
from haversine import haversine
import numpy as np

# point(latitude, longitude)

# Example

def find_distance(point_y, offset):
    box = cell_towers[((cell_towers['lat'] > point_y[0] - offset) & (cell_towers['lat'] < point_y[0] + offset)) & ((cell_towers['lon'] > point_y[1] - offset) & (cell_towers['lon'] < point_y[1] + offset))]
    d = []
    for i in range(len(box)):
        lon = box['lon'].iloc[i]
        lat = box['lat'].iloc[i]
        point_x = (lat, lon)
        d.append(haversine(point_x, point_y))
    
    if d : 
        return min(d) 
    
    return np.nan


min_d = []
for x in range(len(cell_tw)):
    point = (cell_tw['lat'].iloc[x], cell_tw['lon'].iloc[x])
    min_d.append(find_distance(point, offset = 0.2))
    
sum(np.isnan(min_d))

subset = cell_tw[np.isnan(min_d)]
smin_d = []
for x in range(len(subset)):
    point = (subset['lat'].iloc[x], subset['lon'].iloc[x])
    smin_d.append(find_distance(point, offset = 1))

sum(np.isnan(smin_d))