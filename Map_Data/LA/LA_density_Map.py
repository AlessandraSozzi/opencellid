import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize
from matplotlib.collections import PatchCollection
from mpl_toolkits.basemap import Basemap
from shapely.geometry import Point, Polygon, MultiPoint, MultiPolygon
from shapely.prepared import prep
from pysal.esda.mapclassify import Natural_Breaks as nb
from pysal.esda.mapclassify import Quantiles
from descartes import PolygonPatch
import fiona
from itertools import chain
import osgeo.ogr

# Convenience functions for working with colour ramps and bars
def colorbar_index(ncolors, cmap, labels=None, **kwargs):
    """
    This is a convenience function to stop you making off-by-one errors
    Takes a standard colour ramp, and discretizes it,
    then draws a colour bar with correctly aligned labels
    """
    cmap = cmap_discretize(cmap, ncolors)
    mappable = cm.ScalarMappable(cmap=cmap)
    mappable.set_array([])
    mappable.set_clim(-0.5, ncolors + 0.5)
    colorbar = plt.colorbar(mappable, **kwargs)
    colorbar.set_ticks(np.linspace(0, ncolors, ncolors))
    colorbar.set_ticklabels(range(ncolors))
    if labels:
        colorbar.set_ticklabels(labels)
    return colorbar

def cmap_discretize(cmap, N):
    """
    Return a discrete colormap from the continuous colormap cmap.

        cmap: colormap instance, eg. cm.jet. 
        N: number of colors.

    Example
        x = resize(arange(100), (5,100))
        djet = cmap_discretize(cm.jet, 5)
        imshow(x, cmap = djet)

    """
    if type(cmap) == str:
        cmap = get_cmap(cmap)
    colors_i = np.concatenate((np.linspace(0, 1., N), (0., 0., 0., 0.)))
    colors_rgba = cmap(colors_i)
    indices = np.linspace(0, 1., N + 1)
    cdict = {}
    for ki, key in enumerate(('red', 'green', 'blue')):
        cdict[key] = [(indices[i], colors_rgba[i - 1, ki], colors_rgba[i, ki]) for i in xrange(N + 1)]
    return matplotlib.colors.LinearSegmentedColormap(cmap.name + "_%d" % N, cdict, 1024)


##################################################################################################
###################           LA N. towers x 1000 people        #####################################
##################################################################################################

LA_df = pd.read_csv('/Users/Alessandra/GitHub/OpenCellID/Map_Data/LA/totPopCt.csv')

shp = fiona.open('LA2014.shp')
bds = shp.bounds # extract map boundaries
shp.close()

ll = (bds[0], bds[1])
ur = (bds[2], bds[3])
coords = list(chain(ll, ur))

# Create a Basemap instance, which we can use to plot the maps on
m = Basemap(
    projection = 'tmerc',
    lon_0 = -4.36, lat_0 = 54.7,
    ellps = 'WGS84',
    llcrnrlon= -10.5, llcrnrlat = 49.5, urcrnrlon= 3.5, urcrnrlat = 59.5,
    lat_ts = 0,
    resolution = 'i',
    suppress_ticks = True) # suppress automatic drawing of axis ticks and labels in map projection coordinates
    
m.readshapefile(
    'LA2014',
    'LA2014',
    color = 'none',
    zorder = 2);
    
# Set up a map dataframe
df_map = pd.DataFrame({
    'poly': [Polygon(xy) for xy in m.LA2014],
    'code': [area['LAD14CD'] for area in m.LA2014_info],
    'name': [area['LAD14NM'] for area in m.LA2014_info]})

df_map = pd.merge(df_map, LA_df, on = "code", how = "left")



breaks = nb(
    df_map[df_map['x1000'].notnull()].x1000.values,
    initial = 300, # number of initial solutions to generate
    k = 7) # number of classes required
# The notnull method lets match indices when joining
jb = pd.DataFrame({'jenks_bins': breaks.yb}, index = df_map[df_map['x1000'].notnull()].index)
df_map = df_map.join(jb)
df_map.jenks_bins.fillna(-1, inplace=True)

# Create a sensible label for classes
jenks_labels = ["<= %0.3f ct x1000" % (b) for b in breaks.bins]
jenks_labels.insert(0, 'No data')

# Create a Basemap instance, which we can use to plot the maps on
m = Basemap(
    projection = 'tmerc',
    lon_0 = -4.36, lat_0 = 55.7,
    ellps = 'WGS84',
    llcrnrlon= -10.5, llcrnrlat = 49.5, urcrnrlon= 3.5, urcrnrlat = 60.5,
    lat_ts = 0,
    resolution = 'i',
    suppress_ticks = True) # suppress automatic drawing of axis ticks and labels in map projection coordinates
    
m.readshapefile(
    'LA2014',
    'LA2014',
    color = 'none',
    zorder = 2);


plt.clf()
fig = plt.figure()
ax = fig.add_subplot(111, axisbg= 'w', frame_on = False)

# Use a blue colour ramp
cmap = plt.get_cmap('Blues')

# Draw squares with grey outlines
df_map['patches'] = df_map['poly'].map(lambda x: PolygonPatch(x, ec = '#555555', lw = .2, alpha = 1., zorder = 4))
pc = PatchCollection(df_map['patches'], match_original = True)

# Impose colour map onto the patch collection
norm = Normalize()
pc.set_facecolor(cmap(norm(df_map['jenks_bins'].values)))
ax.add_collection(pc)

# Add a colour bar
cb = colorbar_index(ncolors = len(jenks_labels), cmap = cmap, shrink = 0.5, labels = jenks_labels)
cb.ax.tick_params(labelsize = 9)

    
# Draw a map scale
m.drawmapscale(
    coords[0] - 1.5, coords[1] - 1.5,
    coords[0], coords[1],
    1.,
    barstyle = 'fancy', labelstyle = 'simple',
    fillcolor1 = 'w', fillcolor2 = '#555555',
    fontcolor = '#555555',
    zorder = 5)

# Set the image width to 722px at 100dpi
plt.title("Cell towers x 1000 residents")
plt.tight_layout()
fig.set_size_inches(7.22, 6)
plt.savefig('/Users/Alessandra/GitHub/OpenCellID/Map_Data/LA/LA_Plots/x1000LA.png', dpi = 100, alpha = True)
plt.show()