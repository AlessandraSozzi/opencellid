import pandas as pd
import osgeo.ogr

# Look at the shapefile content
shapefileLSOA = osgeo.ogr.Open('LSOA_EW_ct.shp')
shapefileLSOA.GetLayerCount()
layer = shapefileLSOA.GetLayer(0)
spatialRef = layer.GetSpatialRef().ExportToProj4()
numFeatures = layer.GetFeatureCount()
feature = layer.GetFeature(0)
attributes = feature.items()
for key,value in attributes.items():
    print " %s = %s" % (key, value)

df_LSOA_map = pd.DataFrame(columns=('code', 'name', 'ct_count'))   

# Set up a map dataframe
for i in range(numFeatures):
    feature = layer.GetFeature(i)
    attributes = feature.items()
    df_LSOA_map.loc[i] = [attributes['LSOA11CD'], attributes['LSOA11NM'], attributes['PNTCNT']]

df_LSOA_map = df_LSOA_map.groupby(['code', 'name'], as_index=False).sum() # 34,753
# Number of missing values for each column
[sum(pd.isnull(df_LSOA_map[colname])) for colname in list(df_LSOA_map.columns.values)]

totPopLSOA_res = pd.read_csv('/Users/Alessandra/GitHub/OpenCellID/Map_Data/LSOA/usual_residents_2011/usual_residents_2011.csv')
totPopLSOA_wd = pd.read_csv('/Users/Alessandra/GitHub/OpenCellID/Map_Data/LSOA/workday_population_2011/LSOA_workdayPopulation.csv')
totPopLSOA_res = pd.merge(totPopLSOA_res, totPopLSOA_wd[['code', 'count']], on = "code", how = "left")
totPopLSOA_res.drop(['density', 'name'], axis = 1, inplace = True)
totPopLSOA_res.columns = ['code', "res_popn_count", "hectares", "wd_popn_count"]
totPopLSOA_res['max_pop'] = totPopLSOA_res[["res_popn_count","wd_popn_count"]].max(axis=1)

totPopLSOA = pd.merge(totPopLSOA_res, df_LSOA_map, on = "code", how = "left")
totPopLSOA.to_csv("/Users/Alessandra/GitHub/OpenCellID/Map_Data/LSOA/totPopLSOA.csv", index = False)

# Number of missing values for each column
[sum(pd.isnull(totPopLSOA[colname])) for colname in list(totPopLSOA.columns.values)]
# [0, 0, 0, 0, 0, 0, 1150]

################## After 2012

# Look at the shapefile content
shapefileLSOA = osgeo.ogr.Open('LSOA_EW_ct_2012.shp')
shapefileLSOA.GetLayerCount()
layer = shapefileLSOA.GetLayer(0)
spatialRef = layer.GetSpatialRef().ExportToProj4()
numFeatures = layer.GetFeatureCount()
feature = layer.GetFeature(0)
attributes = feature.items()
for key,value in attributes.items():
    print " %s = %s" % (key, value)

df_LSOA_map_2012 = pd.DataFrame(columns=('code', 'name', 'ct_count'))   

# Set up a map dataframe
for i in range(numFeatures):
    feature = layer.GetFeature(i)
    attributes = feature.items()
    df_LSOA_map_2012.loc[i] = [attributes['LSOA11CD'], attributes['LSOA11NM'], attributes['CNT_2012']]
    
df_LSOA_map_2012['region'] = [x[:1] for x in df_LSOA_map_2012['code']]
df_LSOA_map_2012.ct_count.fillna(0, inplace=True)

  
E = df_LSOA_map_2012[df_LSOA_map_2012['region'] == "E"]
W = df_LSOA_map_2012[df_LSOA_map_2012['region'] == "W"]
sum(E['ct_count'] > 0)/float(len(E))
sum(W['ct_count'] > 0)/float(len(W))

LSOA_London_df = pd.merge(LSOA_London_df[['code','max_pop']], df_LSOA_map_2012, on='code', how = 'left')
