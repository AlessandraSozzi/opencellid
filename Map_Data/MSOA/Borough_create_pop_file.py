import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize
from matplotlib.collections import PatchCollection
from mpl_toolkits.basemap import Basemap
from shapely.geometry import Point, Polygon, MultiPoint, MultiPolygon
from shapely.prepared import prep
from pysal.esda.mapclassify import Natural_Breaks as nb
from pysal.esda.mapclassify import Quantiles
from descartes import PolygonPatch
import fiona
from itertools import chain
import osgeo.ogr

# Look at the shapefile content

shapefile = osgeo.ogr.Open('/Users/Alessandra/GitHub/OpenCellID/Map_Data/Borough/London_Borough_with_ct.shp')
shapefile.GetLayerCount()
layer = shapefile.GetLayer(0)
spatialRef = layer.GetSpatialRef().ExportToProj4()
numFeatures = layer.GetFeatureCount()
feature = layer.GetFeature(0)
attributes = feature.items()
for key,value in attributes.items():
    print " %s = %s" % (key, value)

df_map = pd.DataFrame(columns=('code', 'name', 'ct_count'))   

# Set up a map dataframe
for i in range(numFeatures):
    feature = layer.GetFeature(i)
    attributes = feature.items()
    df_map.loc[i] = [attributes['GSS_CODE'], attributes['NAME'], attributes['PNTCNT']]

df_map = df_map.groupby(['code', 'name'], as_index=False).sum()
totPopBorough = pd.read_csv('/Users/Alessandra/GitHub/OpenCellID/Map_Data/Borough/daytime_pop.csv')
totPopBorough = pd.merge(totPopBorough, df_map[['code', 'ct_count']], on = "code", how = "left")
resPopBorough = pd.read_csv('/Users/Alessandra/GitHub/OpenCellID/Map_Data/Borough/2014_borough_res_pop.csv')
totPopBorough = pd.merge(totPopBorough, resPopBorough[['Code', 'All Persons']], left_on = "code", right_on = "Code", how = "left")
totPopBorough.drop("Code", axis = 1, inplace = True)
totPopBorough.columns = ['code', "name", "daytime_pop", "ct_count", "res_pop"]
totPopBorough["max_pop"] = totPopBorough[["daytime_pop","res_pop"]].max(axis=1)
totPopBorough.to_csv("/Users/Alessandra/GitHub/OpenCellID/Map_Data/Borough/totPopBorough.csv", index = False)
