import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize
from matplotlib.collections import PatchCollection
from mpl_toolkits.basemap import Basemap
from shapely.geometry import Point, Polygon, MultiPoint, MultiPolygon
from shapely.prepared import prep
from pysal.esda.mapclassify import Natural_Breaks as nb
from pysal.esda.mapclassify import Quantiles
from descartes import PolygonPatch
import fiona
from itertools import chain
import osgeo.ogr

# Look at the shapefile content
shapefile = osgeo.ogr.Open('MSOA_EW_ct.shp')
shapefile.GetLayerCount()
layer = shapefile.GetLayer(0)
spatialRef = layer.GetSpatialRef().ExportToProj4()
numFeatures = layer.GetFeatureCount()
feature = layer.GetFeature(0)
attributes = feature.items()
for key,value in attributes.items():
    print " %s = %s" % (key, value)

df_map = pd.DataFrame(columns=('code', 'name', 'ct_count'))   

# Set up a map dataframe
for i in range(numFeatures):
    feature = layer.GetFeature(i)
    attributes = feature.items()
    df_map.loc[i] = [attributes['MSOA11CD'], attributes['MSOA11NM'], attributes['PNTCNT']]

df_map = df_map.groupby(['code', 'name'], as_index=False).sum()
totPopMSOA = pd.read_csv('/Users/Alessandra/GitHub/OpenCellID/Map_Data/MSOA/totPopMSOA.csv')
totPopMSOA = pd.merge(totPopMSOA, df_map[['code', 'ct_count']], on = "code", how = "left")
totPopMSOA.to_csv("/Users/Alessandra/GitHub/OpenCellID/Map_Data/MSOA/totPopMSOA.csv", index = False)
