import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize
from matplotlib.collections import PatchCollection
from mpl_toolkits.basemap import Basemap
from shapely.geometry import Point, Polygon, MultiPoint, MultiPolygon
from shapely.prepared import prep
from pysal.esda.mapclassify import Natural_Breaks as nb
from pysal.esda.mapclassify import Quantiles
from descartes import PolygonPatch
import fiona
from itertools import chain
import osgeo.ogr

# Convenience functions for working with colour ramps and bars
def colorbar_index(ncolors, cmap, labels=None, **kwargs):
    """
    This is a convenience function to stop you making off-by-one errors
    Takes a standard colour ramp, and discretizes it,
    then draws a colour bar with correctly aligned labels
    """
    cmap = cmap_discretize(cmap, ncolors)
    mappable = cm.ScalarMappable(cmap=cmap)
    mappable.set_array([])
    mappable.set_clim(-0.5, ncolors + 0.5)
    colorbar = plt.colorbar(mappable, **kwargs)
    colorbar.set_ticks(np.linspace(0, ncolors, ncolors))
    colorbar.set_ticklabels(range(ncolors))
    if labels:
        colorbar.set_ticklabels(labels)
    return colorbar

def cmap_discretize(cmap, N):
    """
    Return a discrete colormap from the continuous colormap cmap.

        cmap: colormap instance, eg. cm.jet. 
        N: number of colors.

    Example
        x = resize(arange(100), (5,100))
        djet = cmap_discretize(cm.jet, 5)
        imshow(x, cmap = djet)

    """
    if type(cmap) == str:
        cmap = get_cmap(cmap)
    colors_i = np.concatenate((np.linspace(0, 1., N), (0., 0., 0., 0.)))
    colors_rgba = cmap(colors_i)
    indices = np.linspace(0, 1., N + 1)
    cdict = {}
    for ki, key in enumerate(('red', 'green', 'blue')):
        cdict[key] = [(indices[i], colors_rgba[i - 1, ki], colors_rgba[i, ki]) for i in xrange(N + 1)]
    return matplotlib.colors.LinearSegmentedColormap(cmap.name + "_%d" % N, cdict, 1024)


##################################################################################################
###################    MSOA London N. towers x 1000 people    #####################################
##################################################################################################

MSOA_London_df = pd.read_csv('/Users/Alessandra/GitHub/OpenCellID/Map_Data/MSOA/London/totPopMSOA_London.csv')

shp = fiona.open('MSOA.shp')
bds = shp.bounds # extract map boundaries
shp.close()

ll = (bds[0], bds[1])
ur = (bds[2], bds[3])
coords = list(chain(ll, ur))
w, h = coords[2] - coords[0], coords[3] - coords[1]
extra = 0.01


# Create a Basemap instance, which we can use to plot the maps on
m = Basemap(
    projection = 'tmerc',
    lon_0 = -0.1, lat_0 = 51.5,
    ellps = 'WGS84',
    llcrnrlon=coords[0] - extra * w,
    llcrnrlat=coords[1] - extra + 0.01 * h,
    urcrnrlon=coords[2] + extra * w,
    urcrnrlat=coords[3] + extra + 0.01 * h,
    lat_ts = 0,
    resolution = 'i',
    suppress_ticks = True) # suppress automatic drawing of axis ticks and labels in map projection coordinates
    
m.readshapefile(
    'MSOA',
    'MSOA',
    color = 'none',
    zorder = 2);
    
# Set up a map dataframe
df_map = pd.DataFrame({
    'poly': [Polygon(xy) for xy in m.MSOA],
    'code': [area['MSOA11CD'] for area in m.MSOA_info],
    'name': [area['MSOA11NM'] for area in m.MSOA_info]})

df_map = pd.merge(df_map, MSOA_London_df, on = "code", how = "left")
df_map['max_x1000'] = df_map['ct_count']/(df_map['max_pop']/1000.0)


breaks = nb(
    df_map[df_map['max_x1000'].notnull()].max_x1000.values,
    initial = 300, # number of initial solutions to generate
    k = 7) # number of classes required
# The notnull method lets match indices when joining
jb = pd.DataFrame({'jenks_bins': breaks.yb}, index = df_map[df_map['max_x1000'].notnull()].index)
df_map = df_map.join(jb)
df_map.jenks_bins.fillna(-1, inplace=True)

# Create a sensible label for classes
jenks_labels = ["<= %0.3f ct x1000" % (b) for b in breaks.bins]
jenks_labels.insert(0, 'No data')


plt.clf()
fig = plt.figure()
ax = fig.add_subplot(111, axisbg= 'w', frame_on = False)

# Use a blue colour ramp
cmap = plt.get_cmap('Blues')

# Draw squares with grey outlines
df_map['patches'] = df_map['poly'].map(lambda x: PolygonPatch(x, ec = '#555555', lw = .2, alpha = 1., zorder = 4))
pc = PatchCollection(df_map['patches'], match_original = True)

# Impose colour map onto the patch collection
norm = Normalize()
pc.set_facecolor(cmap(norm(df_map['jenks_bins'].values)))
ax.add_collection(pc)

# Add a colour bar
cb = colorbar_index(ncolors = len(jenks_labels), cmap = cmap, shrink = 0.5, labels = jenks_labels)
cb.ax.tick_params(labelsize = 9)

    
# Draw a map scale
m.drawmapscale(
    coords[0] - 1.5, coords[1] - 1.5,
    coords[0], coords[1],
    1.,
    barstyle = 'fancy', labelstyle = 'simple',
    fillcolor1 = 'w', fillcolor2 = '#555555',
    fontcolor = '#555555',
    zorder = 5)

# Set the image width to 722px at 100dpi
plt.title("Cell towers x 1000 max(residents, workday) population")
plt.tight_layout()
fig.set_size_inches(7.22, 6)
plt.savefig('/Users/Alessandra/GitHub/OpenCellID/Map_Data/MSOA/MSOA_London_Plots/x1000MSOA.png', dpi = 100, alpha = True)
plt.show()