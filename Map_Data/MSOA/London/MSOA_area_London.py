import pandas as pd
import osgeo.ogr

# Look at the shapefile content
shapefile_msoa = osgeo.ogr.Open('MSOA.shp')
shapefile_msoa.GetLayerCount()
layer = shapefile_msoa.GetLayer(0)
spatialRef = layer.GetSpatialRef().ExportToProj4()
numFeatures = layer.GetFeatureCount()
feature = layer.GetFeature(0)
attributes = feature.items()
for key,value in attributes.items():
    print " %s = %s" % (key, value)

df_map_msoa = pd.DataFrame(columns=('code', 'name'))   

# Set up a map dataframe
for i in range(numFeatures):
    feature = layer.GetFeature(i)
    attributes = feature.items()
    df_map_msoa.loc[i] = [attributes['MSOA11CD'], attributes['MSOA11NM']]

df_map_msoa.to_csv("/Users/Alessandra/GitHub/OpenCellID/Map_Data/MSOA/London/MSOA_area_London.csv", index = False)

totPopMSOA = pd.read_csv('/Users/Alessandra/GitHub/OpenCellID/Map_Data/MSOA/totPopMSOA.csv')
totPopMSOA.drop('name', axis = 1, inplace = True)

totPopMSOA_London = pd.merge(df_map_msoa, totPopMSOA, on = "code", how = "left")

totPopMSOA_London.to_csv("/Users/Alessandra/GitHub/OpenCellID/Map_Data/MSOA/London/totPopMSOA_London.csv", index = False)