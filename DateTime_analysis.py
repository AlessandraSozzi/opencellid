import pandas as pd
import datetime
from datetime import timedelta, tzinfo

import matplotlib.pyplot as plt
import seaborn; seaborn.set()  # plot styling



iter_csv = pd.read_csv('~/GitHub/OpenCellID/ct_data/cell_towers.csv',
                    header = 0, 
                    sep = ',', 
                    usecols=['radio', 'mcc', 'net', 'cell', 'lon', 'lat', 'range', 'samples', 'changeable', 'created', 'updated'],
                    dtype = {'radio': str, 'mcc': str, 'net': str, 'cell': str, 'lon': float, 'lat': float,
                                'range': float, 'samples': int, 'changeable': int, 'created': long, 'updated': long},
                    iterator = True, chunksize = 100)
cell_towers = pd.concat([chunk[chunk['mcc'].isin(['234', '235'])] for chunk in iter_csv])
# 304,138 number of cell towers in UK


cell_towers['created'] = [datetime.datetime.fromtimestamp(t) - timedelta(hours = 1) for t in cell_towers['created'].values]
cell_towers['updated'] = [datetime.datetime.fromtimestamp(t) - timedelta(hours = 1) for t in cell_towers['updated'].values]
cell_towers['days_diff'] = (datetime.datetime.now() - cell_towers['updated'])/timedelta(days=1)
cell_towers['days_diff_creation'] = (datetime.datetime.now() - cell_towers['created'])/timedelta(days=1)

binBoundaries = np.linspace(0,2800,101)

cell_towers['days_diff_creation'].hist(bins = binBoundaries, label = 'Days from creation', color = (255/255.,127/255.,14/255.))
plt.ylabel('Number of cell towers')
plt.xlabel('Days')
plt.title('Number of days elapsed from creation')
plt.legend(loc = 2);

cell_towers['days_diff'].hist(bins = binBoundaries, label = 'Days from last update', color = (31/255., 119/255., 180/255.))
plt.ylabel('Number of cell towers')
plt.xlabel('Days')
plt.title('Number of days elapsed from latest update')
plt.legend(loc = 2);


cell_towers['abs_diff'].hist(bins = np.linspace(0,2720,101), label = 'Difference in days', color = (148/255.,103/255.,189/255.))
plt.xlim(0, 2720)
plt.ylabel('Number of towers')
plt.xlabel('Difference in days')
plt.title('Day of update - Day of creation')
plt.legend(loc = 2);


created = cell_towers[['created', 'cell']].groupby(['created']).count()
updated = cell_towers[['updated', 'cell']].groupby(['updated']).count()

created.resample('M', how = 'sum').plot(legend = False)
plt.ylabel('Number of new cell towers')
plt.xlabel('')
plt.title('Number of new cell towers per month');

created.resample('A', how = 'sum').plot(legend = False)
plt.ylabel('Number of new cell towers')
plt.xlabel('')
plt.title('Number of new cell towers per year');

cum_sum = created.resample('A', how = 'sum')
cum_sum['cumsum'] = cum_sum['cell'].cumsum()

cum_sum['cumsum'].plot(legend = False)
plt.ylabel('Number of cell towers')
plt.xlabel('')
plt.title('Growth of cell towers per year');

cum_sum['popn'] = [61.81, 62.28, 62.77, 63.26, 63.7, 64.1, np.nan, np.nan]



####Heatmap creation date by year

# Create a subset for each creation year
cell_towers['creation_year'] = [t.year for t in cell_towers['created']]
for year in set(cell_towers['creation_year']):
    filename = 'year' + str(year) + '.csv'
    subset = cell_towers[cell_towers['creation_year'] == year].copy()
    subset.to_csv(filename)
    
# Create a subset for each update year
cell_towers['update_year'] = [t.year for t in cell_towers['updated']]
for year in set(cell_towers['update_year']):
    filename = 'UpdateYear' + str(year) + '.csv'
    subset = cell_towers[cell_towers['update_year'] == year].copy()
    subset.to_csv(filename)



# GIF images
from images2gif import writeGif
from PIL import Image
file_names = ['/Users/Alessandra/GitHub/OpenCellID/Plots/yearly_maps/'+ str(fn) +'.jpeg'  for fn in set(cell_towers['creation_year'])]

images = [Image.open(fn) for fn in file_names]

size = (643, 480)
for img in images:
    img.thumbnail(size, Image.ANTIALIAS)

filename = "/Users/Alessandra/GitHub/OpenCellID/Plots/yearly_maps/yearly_evolution.GIF"
writeGif(filename, images, duration = 0.2)
    
