import pandas as pd
import numpy as np


breaks = []
x = sorted(df_map2['count_x1000'], reverse = True)

def find_breaks(x):
    
    mean = np.mean(x)
    breaks.append(mean)
    head_n = sum(x >= mean)
    
    
    if (len(x) > 1):
        return find_breaks(x[0:head_n])

    return breaks
    
df_map2['htb'] = [0 for i in range(len(df_map2))]
for i in df_map2['count_x1000'].index:
    df_map2['htb'].iloc[i] = sum(df_map2['count_x1000'].iloc[i] > breaks)


