import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize
from matplotlib.collections import PatchCollection
from mpl_toolkits.basemap import Basemap
from shapely.geometry import Point, Polygon, MultiPoint, MultiPolygon
from shapely.prepared import prep
from pysal.esda.mapclassify import Natural_Breaks as nb
from descartes import PolygonPatch
import fiona
from itertools import chain
import osgeo.ogr

# Convenience functions for working with colour ramps and bars
def colorbar_index(ncolors, cmap, labels=None, **kwargs):
    """
    This is a convenience function to stop you making off-by-one errors
    Takes a standard colour ramp, and discretizes it,
    then draws a colour bar with correctly aligned labels
    """
    cmap = cmap_discretize(cmap, ncolors)
    mappable = cm.ScalarMappable(cmap=cmap)
    mappable.set_array([])
    mappable.set_clim(-0.5, ncolors + 0.5)
    colorbar = plt.colorbar(mappable, **kwargs)
    colorbar.set_ticks(np.linspace(0, ncolors, ncolors))
    colorbar.set_ticklabels(range(ncolors))
    if labels:
        colorbar.set_ticklabels(labels)
    return colorbar

def cmap_discretize(cmap, N):
    """
    Return a discrete colormap from the continuous colormap cmap.

        cmap: colormap instance, eg. cm.jet. 
        N: number of colors.

    Example
        x = resize(arange(100), (5,100))
        djet = cmap_discretize(cm.jet, 5)
        imshow(x, cmap = djet)

    """
    if type(cmap) == str:
        cmap = get_cmap(cmap)
    colors_i = np.concatenate((np.linspace(0, 1., N), (0., 0., 0., 0.)))
    colors_rgba = cmap(colors_i)
    indices = np.linspace(0, 1., N + 1)
    cdict = {}
    for ki, key in enumerate(('red', 'green', 'blue')):
        cdict[key] = [(indices[i], colors_rgba[i - 1, ki], colors_rgba[i, ki]) for i in xrange(N + 1)]
    return matplotlib.colors.LinearSegmentedColormap(cmap.name + "_%d" % N, cdict, 1024)

'''

Transformation of shapefile from command line:
ogr2ogr -s_srs http://spatialreference.org/ref/epsg/27700/ -t_srs http://spatialreference.org/ref/epsg/4326/ ~/GitHub/OpenCellID/reprojected/MSOA2011.shp MSOA_2011_EW_BFE_V2.shp
'''
# Look at the shapefile content
shapefile = osgeo.ogr.Open('LA2014.shp')
shapefile.GetLayerCount()
layer = shapefile.GetLayer(0)
spatialRef = layer.GetSpatialRef().ExportToProj4()
numFeatures = layer.GetFeatureCount()
feature = layer.GetFeature(0)
attributes = feature.items()
for key,value in attributes.items():
    print " %s = %s" % (key, value)

shapefile = None

shp = fiona.open('LA2014.shp')
bds = shp.bounds # extract map boundaries
shp.close()

ll = (bds[0], bds[1])
ur = (bds[2], bds[3])
coords = list(chain(ll, ur))

# Create a Basemap instance, which we can use to plot the maps on
m = Basemap(
    projection = 'tmerc',
    lon_0 = -4.36, lat_0 = 54.7,
    ellps = 'WGS84',
    llcrnrlon= -10.5, llcrnrlat = 49.5, urcrnrlon= 3.5, urcrnrlat = 59.5,
    lat_ts = 0,
    resolution = 'i',
    suppress_ticks = True) # suppress automatic drawing of axis ticks and labels in map projection coordinates
    
m.readshapefile(
    'LA2014',
    'LA2014',
    color = 'none',
    zorder = 2);
    
# Set up a map dataframe
df_map = pd.DataFrame({
    'poly': [Polygon(xy) for xy in m.LA2014],
    'code': [area['LAD14CD'] for area in m.LA2014_info],
    'name': [area['LAD14NM'] for area in m.LA2014_info]})

# Standard Area Measurments UK 2014
area = pd.read_csv('/Users/Alessandra/GitHub/OpenCellID/SAM2014/SAM2014v2.csv', 
                    usecols = ['code', 'AREALHECT'],
                    dtype = {'code': str, 'AREALHECT': float})
df_map = pd.merge(df_map, area, how = 'inner', on = 'code') 

# Read cell towers IDs csv
iter_csv = pd.read_csv('~/GitHub/OpenCellID/ct_data/cell_towers.csv',
                    header = 0, 
                    sep = ',', 
                    index_col=['cell'],
                    usecols=['radio', 'mcc', 'net', 'cell', 'lon', 'lat', 'range', 'samples', 'changeable', 'created', 'updated'],
                    dtype = {'radio': str, 'mcc': str, 'net': str, 'cell': str, 'lon': float, 'lat': float,
                                'range': float, 'samples': int, 'changeable': int, 'created': long, 'updated': long},
                    iterator = True, chunksize = 100)
cell_towers = pd.concat([chunk[chunk['mcc'].isin(['234', '235'])] for chunk in iter_csv])
# 304,138 number of cell towers in UK

# Create Point objects in map coordinates from dataframe lon and lat values
map_points = pd.Series(
    [Point(m(mapped_x, mapped_y)) for mapped_x, mapped_y in zip(cell_towers['lon'], cell_towers['lat'])])
tower_points = MultiPoint(list(map_points.values))
adm_polygon = prep(MultiPolygon(list(df_map['poly'].values)))
# calculate points that fall within the boundary
adm_points = filter(adm_polygon.contains, tower_points)
  

df_map['count'] = df_map['poly'].map(lambda x: int(len(filter(prep(x).contains, adm_points)))) # Allocated: 300,026 cell towers

df_map['density_km'] = df_map['count'] / df_map['area_km']
# Replace with NaN values. Easier to work with when classifying
df_map.replace(to_replace={'density_km': {0: np.nan}}, inplace=True)

###### Jenks Classification #####

# Calculate Jenks natural breaks for density
# Classification scheme for choropleth mapping
breaks = nb(
    df_map[df_map['density_km'].notnull()].density_km.values,
    initial = 300, # number of initial solutions to generate
    k = 7) # number of classes required
# The notnull method lets match indices when joining
jb = pd.DataFrame({'jenks_bins': breaks.yb}, index = df_map[df_map['density_km'].notnull()].index)
df_map = df_map.join(jb)
df_map.jenks_bins.fillna(-1, inplace=True)

# Create a sensible label for classes
# Show density/square km, as well as the number of squares in the class
jenks_labels = ["<= %0.3f/km$^2$" % (b) for b in breaks.bins]
jenks_labels.insert(0, 'No data')

plt.clf()
fig = plt.figure()
ax = fig.add_subplot(111, axisbg= 'w', frame_on = False)

# Use a blue colour ramp
cmap = plt.get_cmap('Reds')

# Draw squares with grey outlines
df_map['patches'] = df_map['poly'].map(lambda x: PolygonPatch(x, ec = '#555555', lw = .2, alpha = 1., zorder = 4))
pc = PatchCollection(df_map['patches'], match_original = True)

# Impose colour map onto the patch collection
norm = Normalize()
pc.set_facecolor(cmap(norm(df_map['jenks_bins'].values)))
ax.add_collection(pc)

# Add a colour bar
cb = colorbar_index(ncolors = len(jenks_labels), cmap = cmap, shrink = 0.5, labels = jenks_labels)
cb.ax.tick_params(labelsize = 9)

    
# Draw a map scale
m.drawmapscale(
    coords[0] - 1.5, coords[1] - 1.5,
    coords[0], coords[1],
    1.,
    barstyle = 'fancy', labelstyle = 'simple',
    fillcolor1 = 'w', fillcolor2 = '#555555',
    fontcolor = '#555555',
    zorder = 5)

# Set the image width to 722px at 100dpi
plt.title("Cell Towers density in UK")
plt.tight_layout()
fig.set_size_inches(7.22, 5.25)
plt.savefig('/Users/Alessandra/GitHub/OpenCellID/Plots/CellIDdensity.png', dpi = 100, alpha = True)
plt.show()


# Read population density csv
popn = pd.read_csv('/Users/Alessandra/GitHub/OpenCellID/LA-population-estimates/popn2014.csv',
                    dtype = {'code': str,'name': str,'pop_count': float})

df_map = pd.merge(df_map, popn, how = 'left', on = 'code', sort = False)
df_map['popdensity_km'] = df_map['popn_count'] / df_map['area_km']
# Replace with NaN values. Easier to work with when classifying
df_map.replace(to_replace={'popdensity_km': {0: np.nan}}, inplace=True)

# Calculate Jenks natural breaks for population density
# Classification scheme for choropleth mapping
breaks = nb(
    df_map[df_map['popdensity_km'].notnull()].popdensity_km.values,
    initial = 300, # number of initial solutions to generate
    k = 7) # number of classes required
# The notnull method lets match indices when joining
jb_popn = pd.DataFrame({'jenks_bins_popn': breaks.yb}, index = df_map[df_map['popdensity_km'].notnull()].index)
df_map = df_map.join(jb_popn)
df_map.jenks_bins_popn.fillna(-1, inplace=True)

# Create a sensible label for classes
# Show density/hectare, as well as the number of areas in the class
jenks_labels = ["<= %0.3f/km$^2$" % b for b in breaks.bins]
jenks_labels.insert(0, 'No data')


plt.clf()
fig = plt.figure()
ax = fig.add_subplot(111, axisbg= 'w', frame_on = False)

# Use a blue colour ramp
cmap = plt.get_cmap('Reds')

# Draw squares with grey outlines
df_map['patches'] = df_map['poly'].map(lambda x: PolygonPatch(x, ec = '#555555', lw = .2, alpha = 1., zorder = 4))
pc = PatchCollection(df_map['patches'], match_original = True)

# Impose colour map onto the patch collection
norm = Normalize()
pc.set_facecolor(cmap(norm(df_map['jenks_bins_popn'].values)))
ax.add_collection(pc)

# Add a colour bar
cb = colorbar_index(ncolors = len(jenks_labels), cmap = cmap, shrink = 0.5, labels = jenks_labels)
cb.ax.tick_params(labelsize = 9)

    
# Draw a map scale
m.drawmapscale(
    coords[0] - 1.5, coords[1] - 1.5,
    coords[0], coords[1],
    1.,
    barstyle = 'fancy', labelstyle = 'simple',
    fillcolor1 = 'w', fillcolor2 = '#555555',
    fontcolor = '#555555',
    zorder = 5)

# Set the image width to 722px at 100dpi
plt.title("Population density in Great Britain")
plt.tight_layout()
fig.set_size_inches(7.22, 5.25)
plt.savefig('PopnDdensity.png', dpi = 100, alpha = True)
plt.show()





##### Combination of BOTH
df_map.to_csv('df_map.csv')
new = df_map.drop(['density'], axis = 1, inplace = False)


df_map['popn_cell'] = df_map['density']/df_map['density_hc']# Number of people per cell
subset_c = set(df_map.code.values).intersection(set(popn.code.values))

subset = pd.merge(new, popn, how = 'inner', on = 'code', sort = False)


f = lambda x: subset['code'].tolist().count(x)

for i in set(subset['code']):
    print i + ' ' + str(f(i))

subset.replace(to_replace={'density_hc': {0: np.nan}, 'density': {0: np.nan}}, inplace=True)
subset['popn_cell'] = subset['density']/subset['density_hc']



# Calculate Jenks natural breaks for population density
# Classification scheme for choropleth mapping
breaks = nb(
    subset[subset['popn_cell'].notnull()].popn_cell.values,
    initial = 300, # number of initial solutions to generate
    k = 7) # number of classes required
# The notnull method lets match indices when joining
jb_pc = pd.DataFrame({'jenks_bins_pc': breaks.yb}, index = subset[subset['popn_cell'].notnull()].index)
subset = subset.join(jb_pc)
subset.jenks_bins_pc.fillna(-1, inplace=True)

# Create a sensible label for classes
# Show density/hectare, as well as the number of areas in the class
jenks_labels = ["<= %0.0f residents per cell" % (round(b)) for b in breaks.bins]
jenks_labels.insert(0, 'No data')

# Create a Basemap instance, which we can use to plot the maps on
m = Basemap(
    projection = 'tmerc',
    lon_0 = -4.36, lat_0 = 52.8,
    ellps = 'WGS84',
    llcrnrlon= -10.5, llcrnrlat = 48.8, urcrnrlon= 3.5, urcrnrlat = 56.2,
    lat_ts = 0,
    resolution = 'i',
    suppress_ticks = True) # suppress automatic drawing of axis ticks and labels in map projection coordinates
    
m.readshapefile(
    'county',
    'county',
    color = 'none',
    zorder = 2)


plt.clf()
fig = plt.figure()
ax = fig.add_subplot(111, axisbg= 'w', frame_on = False)

# Use a blue colour ramp
cmap = plt.get_cmap('Reds')

# Draw squares with grey outlines
subset['patches'] = subset['poly'].map(lambda x: PolygonPatch(x, ec = '#555555', lw = .2, alpha = 1., zorder = 4))
pc = PatchCollection(subset['patches'], match_original = True)

# Impose colour map onto the patch collection
norm = Normalize()
pc.set_facecolor(cmap(norm(subset['jenks_bins_pc'].values)))
ax.add_collection(pc)

# Add a colour bar
cb = colorbar_index(ncolors = len(jenks_labels), cmap = cmap, shrink = 0.5, labels = jenks_labels)
cb.ax.tick_params(labelsize = 9)

    
# Draw a map scale
m.drawmapscale(
    coords[0] - 2.5, coords[1] - 2.5,
    coords[0], coords[1],
    1.,
    barstyle = 'fancy', labelstyle = 'simple',
    fillcolor1 = 'w', fillcolor2 = '#555555',
    fontcolor = '#555555',
    zorder = 5)

# Set the image width to 722px at 100dpi
plt.title("Residents per cell in UK")
plt.tight_layout()
fig.set_size_inches(7.24, 5.25)
plt.savefig('ResidentsPerCell.png', dpi = 100, alpha = True)
plt.show()






##################################################################################################
###################           N. towers x 1000 people        #####################################
##################################################################################################

la_areas = df_map[['code', 'count']].groupby('code', as_index = False).sum()

merged = pd.merge(la_areas, popn, how = 'left', on = 'code')

merged['x1000'] = merged['count']/(merged['popn_count']/1000.0)

df_map = pd.merge(df_map, merged[['code', 'x1000']], how = 'left', on = 'code')


breaks = nb(
    df_map[df_map['x1000'] < 100].x1000.values,
    initial = 300, # number of initial solutions to generate
    k = 7) # number of classes required
# The notnull method lets match indices when joining
jb_pc = pd.DataFrame({'jenks_bins_pc': breaks.yb}, index = df_map[df_map['x1000'] < 100].index)
df_map2 = df_map.join(jb_pc)
df_map2.jenks_bins_pc.fillna(-1, inplace=True)

# Create a sensible label for classes
# Show density/hectare, as well as the number of areas in the class
jenks_labels = ["<= %0.2f towers x 1000p" % b for b in breaks.bins]
jenks_labels.insert(0, 'No data')
jenks_labels = jenks_labels + ["<= 130.7 towers x 1000p"]

# Create a Basemap instance, which we can use to plot the maps on
m = Basemap(
    projection = 'tmerc',
    lon_0 = -4.36, lat_0 = 55.7,
    ellps = 'WGS84',
    llcrnrlon= -10.5, llcrnrlat = 49.5, urcrnrlon= 3.5, urcrnrlat = 60.5,
    lat_ts = 0,
    resolution = 'i',
    suppress_ticks = True) # suppress automatic drawing of axis ticks and labels in map projection coordinates
    
m.readshapefile(
    'LA2014',
    'LA2014',
    color = 'none',
    zorder = 2);


plt.clf()
fig = plt.figure()
ax = fig.add_subplot(111, axisbg= 'w', frame_on = False)

# Use a blue colour ramp
cmap = plt.get_cmap('Blues')

# Draw squares with grey outlines
df_map2['patches'] = df_map2['poly'].map(lambda x: PolygonPatch(x, ec = '#555555', lw = .2, alpha = 1., zorder = 4))
pc = PatchCollection(df_map2['patches'], match_original = True)

# Impose colour map onto the patch collection
norm = Normalize()
pc.set_facecolor(cmap(norm(df_map2['jenks_bins_pc'].values)))
ax.add_collection(pc)

# Add a colour bar
cb = colorbar_index(ncolors = len(jenks_labels), cmap = cmap, shrink = 0.5, labels = jenks_labels)
cb.ax.tick_params(labelsize = 9)

    
# Draw a map scale
m.drawmapscale(
    coords[0] - 1.5, coords[1] - 1.5,
    coords[0], coords[1],
    1.,
    barstyle = 'fancy', labelstyle = 'simple',
    fillcolor1 = 'w', fillcolor2 = '#555555',
    fontcolor = '#555555',
    zorder = 5)

# Set the image width to 722px at 100dpi
plt.title("Cell towers x 1000 residents")
plt.tight_layout()
fig.set_size_inches(7.22, 6)
plt.savefig('/Users/Alessandra/GitHub/OpenCellID/Plots/x1000LA.png', dpi = 100, alpha = True)
plt.show()


##################################################################################################
###################           Only last 3 years data         #####################################
##################################################################################################

# Create Point objects in map coordinates from dataframe lon and lat values
last3Years = cell_towers[cell_towers['days_diff'] < 1095].copy()

map_points2 = pd.Series(
    [Point(m(mapped_x, mapped_y)) for mapped_x, mapped_y in zip(last3Years['lon'], last3Years['lat'])])
tower_points2 = MultiPoint(list(map_points2.values))
adm_polygon = prep(MultiPolygon(list(df_map['poly'].values)))
# calculate points that fall within the boundary
adm_points2 = filter(adm_polygon.contains, tower_points2)

df_map['last3YearsCount'] = df_map['poly'].map(lambda x: int(len(filter(prep(x).contains, adm_points2))))


la_areas2 = df_map[['code', 'last3YearsCount']].groupby('code', as_index = False).sum()

merged2 = pd.merge(la_areas2, popn, how = 'left', on = 'code')

merged2['x1000last3Years'] = merged2['last3YearsCount']/(merged2['popn_count']/1000.0)

df_map = pd.merge(df_map, merged2[['code', 'x1000last3Years']], how = 'left', on = 'code')

df_map['updates_bin'] = [-1 for i in range(len(df_map))]
df_map['updates_bin'] = [0 if (x <= 1.315) else df_map['updates_bin'].iloc[i] for i, x in enumerate(df_map['x1000last3Years'])]
df_map['updates_bin'] = [1 if ((x > 1.315) & (x <= 2.723)) else df_map['updates_bin'].iloc[i] for i, x in enumerate(df_map['x1000last3Years'])]
df_map['updates_bin'] = [2 if ((x > 2.723) & (x <= 3.891)) else df_map['updates_bin'].iloc[i] for i, x in enumerate(df_map['x1000last3Years'])]
df_map['updates_bin'] = [3 if ((x > 3.891) & (x <= 5.766)) else df_map['updates_bin'].iloc[i] for i, x in enumerate(df_map['x1000last3Years'])]
df_map['updates_bin'] = [4 if ((x > 5.766) & (x <= 22.162)) else df_map['updates_bin'].iloc[i] for i, x in enumerate(df_map['x1000last3Years'])]
df_map['updates_bin'] = [5 if (x > 22.162) else df_map['updates_bin'].iloc[i] for i, x in enumerate(df_map['x1000last3Years'])]

plt.clf()
fig = plt.figure()
ax = fig.add_subplot(111, axisbg= 'w', frame_on = False)

# Use a blue colour ramp
cmap = plt.get_cmap('Blues')

# Draw squares with grey outlines
df_map['patches'] = df_map['poly'].map(lambda x: PolygonPatch(x, ec = '#555555', lw = .2, alpha = 1., zorder = 4))
pc = PatchCollection(df_map['patches'], match_original = True)

# Impose colour map onto the patch collection
norm = Normalize()
pc.set_facecolor(cmap(norm(df_map['updates_bin'].values)))
ax.add_collection(pc)

# Add a colour bar
cb = colorbar_index(ncolors = len(jenks_labels), cmap = cmap, shrink = 0.5, labels = jenks_labels)
cb.ax.tick_params(labelsize = 9)

    
# Draw a map scale
m.drawmapscale(
    coords[0] - 1.5, coords[1] - 1.5,
    coords[0], coords[1],
    1.,
    barstyle = 'fancy', labelstyle = 'simple',
    fillcolor1 = 'w', fillcolor2 = '#555555',
    fontcolor = '#555555',
    zorder = 5)

# Set the image width to 722px at 100dpi
plt.title("Cell towers x 1000 residents last 3 years data")
plt.tight_layout()
fig.set_size_inches(7.22, 6)
plt.savefig('/Users/Alessandra/GitHub/OpenCellID/Plots/x1000LA_3years.png', dpi = 100, alpha = True)
plt.show()

